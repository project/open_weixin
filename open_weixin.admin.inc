<?php
/**
 * @file
 */

/**
 * AppID and AppSecret of WeChat Open Platform configuration form.
 *
 * @param $form
 * @param $form_state
 */
function open_weixin_admin_form($form, &$form_state) {

  global $base_url;

  $open_weixin_app_id = open_weixin_array_variable_get(OPEN_WEIXIN__VARIABLES, 'open_weixin_app_id', '');
  $open_weixin_app_secret = open_weixin_array_variable_get(OPEN_WEIXIN__VARIABLES, 'open_weixin_app_secret', '');
  $open_weixin_landing_login = open_weixin_array_variable_get(OPEN_WEIXIN__VARIABLES, 'open_weixin_landing_login', 'user');
  $open_weixin_langding_register = open_weixin_array_variable_get(OPEN_WEIXIN__VARIABLES, 'open_weixin_langding_register', 'open-weixin/user-info');

  // Prompt domain need be configured in WeChat Open Platform.
  $domain = isset($_SERVER['SERVER_NAME'])?$_SERVER['SERVER_NAME']:$base_url;

  $form['open_weixin_help'] = array(
    '#markup' => t('Domain name of current site (!domain) need be configured in related entry on WeChat Open Platform.',
      array(
        '!domain' => $domain,
      )
    ),
  );

  $form['open_weixin_nickname'] =array(
    '#type' => 'checkboxes',
    '#options' => drupal_map_assoc(array(t(OPEN_WEIXIN__TEXT__USE_OPEN_WEIXIN_NICKNAME))),
    '#description' => t('Select to auto !configure realname to refer Open Weixin customized nickname: !token.', array(
      '!configure' => l('configure', 'admin/config/people/realname'),
      '!token' => OPEN_WEIXIN__TOKEN__CUSTOM_NICKNAME
    )),
  );

  // Leverages realname variable to judge if use open weixin custom nickname.s
  $use_open_weixin_nickname = variable_get('realname_pattern', '');
  if ($use_open_weixin_nickname == OPEN_WEIXIN__TOKEN__CUSTOM_NICKNAME){
    $form['open_weixin_nickname']['#default_value'] = array(
      t(OPEN_WEIXIN__TEXT__USE_OPEN_WEIXIN_NICKNAME) => t(OPEN_WEIXIN__TEXT__USE_OPEN_WEIXIN_NICKNAME),
    );
  }

  $form['open_weixin_app_id'] = array(
    '#title' => t('AppID'),
    '#type' => 'textfield',
    '#default_value' => $open_weixin_app_id,
  );

  $form['open_weixin_app_secret'] = array(
    '#title' => t('AppSecret'),
    '#type' => 'textfield',
    '#default_value' => $open_weixin_app_secret,
  );

  $form['open_weixin_landing_login'] = array(
    '#title' => t('Login landing page'),
    '#type' => 'textfield',
    '#default_value' => $open_weixin_landing_login,
    '#description' => t('Specifies landing page URL after user login'),
  );

  $form['open_weixin_landing_register'] = array(
    '#title' => t('Register landing page'),
    '#type' => 'textfield',
    '#default_value' => $open_weixin_langding_register,
    '#description' => t('Specifies landing page URL after user register, module provides open-weixin/user-info to let user specify nickname he want to display as username.'),
  );

  $form = system_settings_form($form);
  $form['#submit'] = array('open_weixin_admin_form_submit');

  return $form;
}

/**
 * Save AppID and AppSecret of WeChat Open Platform.
 *
 * @param $form
 * @param $form_state
 */
function open_weixin_admin_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $current_realname_pattern = variable_get('realname_pattern', '[user:name-raw]');

  // Option on
  if (is_array($values['open_weixin_nickname']) && (($values['open_weixin_nickname'][t(OPEN_WEIXIN__TEXT__USE_OPEN_WEIXIN_NICKNAME)]) === t(OPEN_WEIXIN__TEXT__USE_OPEN_WEIXIN_NICKNAME)) ){
    // Only when original realname_pattern is not custom nickname token, save current setting and set custom token.
    if ($current_realname_pattern !== OPEN_WEIXIN__TOKEN__CUSTOM_NICKNAME){
      variable_set('realname_pattern', OPEN_WEIXIN__TOKEN__CUSTOM_NICKNAME);
      open_weixin_array_variable_set(OPEN_WEIXIN__VARIABLES, 'open_weixin_original_realname_pattern', $current_realname_pattern);
    }
  }
  // Option off
  else {
    if ($current_realname_pattern === OPEN_WEIXIN__TOKEN__CUSTOM_NICKNAME){
      variable_set('realname_pattern', open_weixin_array_variable_get(OPEN_WEIXIN__VARIABLES, 'open_weixin_original_realname_pattern', '[user:name-raw]'));
    }
  }

  if (isset($values['open_weixin_app_id'])) {
    open_weixin_array_variable_set(OPEN_WEIXIN__VARIABLES, 'open_weixin_app_id', $values['open_weixin_app_id']);
  }

  if (isset($values['open_weixin_app_secret'])) {
    open_weixin_array_variable_set(OPEN_WEIXIN__VARIABLES, 'open_weixin_app_secret', $values['open_weixin_app_secret']);
  }

  if (isset($values['open_weixin_landing_login'])) {
    open_weixin_array_variable_set(OPEN_WEIXIN__VARIABLES, 'open_weixin_landing_login', $values['open_weixin_landing_login']);
  }

  if (isset($values['open_weixin_landing_register'])) {
    open_weixin_array_variable_set(OPEN_WEIXIN__VARIABLES, 'open_weixin_landing_register', $values['open_weixin_landing_register']);
  }

  drupal_set_message(t('The configuration options have been saved.'));
}
