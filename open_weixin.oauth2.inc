<?php
/**
 * @file
 */

/**
 * If user hasn't login, redirect to WeChat Open Platform to login;
 * if user login, redirect to user homepage.
 */
function _open_weixin_login() {
  // If user has been login, forward to user page.
  $open_weixin_landing_login = open_weixin_array_variable_get(OPEN_WEIXIN__VARIABLES, 'open_weixin_landing_login', 'user');
  if (user_is_logged_in()) {
    drupal_goto($open_weixin_landing_login);
  }
  else {
    // Before OAuth2 authentication, check if necessary configurations are ready.
    // If AppID was not configured, return service unavailable message and leave message in drupal logs.
    $open_weixin_app_id = open_weixin_array_variable_get(OPEN_WEIXIN__VARIABLES, 'open_weixin_app_id', '');

    if (empty($open_weixin_app_id)) {
      print(t(OPEN_WEIXIN__MSG__SERVICE_IS_NA));

      watchdog(
        __FUNCTION__,
        t('Need configure AppID and AppSecret of WeChat Open Platform on !LINK page'),
        array('!LINK' => l(t('config'), 'admin/config/people/open-weixin')),
        WATCHDOG_CRITICAL
      );

    }
    else {
      // Send authentication request to WeChat Open Platform
      drupal_goto(
        format_string(
          OPEN_WEIXIN__API__OAUTH2_REQUEST,
          array(
            '@APPID' => $open_weixin_app_id,
            '@REDIRECT_URI' => url('open-weixin/callback', array('absolute' => TRUE)),
            '@STATE' => open_weixin_state_2_uuid(OPEN_WEIXIN__OAUTH2_STATE__LOGIN)
          )
        )
      );
    }
  }

  return MENU_SITE_ONLINE;
}

/**
 * Error page for details displaying.
 */
function _open_weixin_error() {

}

/**
 * Handles user requests redirected from WeChat Open Platform.
 *
 * If openid exists, treated it as returned user.
 * If unionid exists, treated it as returned user, associated with openid and AppID.
 * otherwise, created new user if user is not login, associated with openid and AppID and reflect information from open weixin into profile.
 */
function _open_weixin_callback() {

  $open_weixin_app_id = open_weixin_array_variable_get(OPEN_WEIXIN__VARIABLES, 'open_weixin_app_id', '');
  $open_weixin_app_secret = open_weixin_array_variable_get(OPEN_WEIXIN__VARIABLES, 'open_weixin_app_secret', '');
  $open_weixin_landing_login = open_weixin_array_variable_get(OPEN_WEIXIN__VARIABLES, 'open_weixin_landing_login', 'user');
  $open_weixin_langding_register = open_weixin_array_variable_get(OPEN_WEIXIN__VARIABLES, 'open_weixin_langding_register', 'open-weixin/user-info');

  try {

    try {
      /* Return User: OpenID exits. */

      // Check if state and code are ready.
      $state = isset($_GET['state']) ? $_GET['state'] : '';
      $state = open_weixin_uuid_2_state($state);

      if (empty($state)) {
        throw new InvalidArgumentException('No OAuth State.');
      }

      // Extract cache data for easily using.
      if (isset($state->data)){
        $state = $state->data;
      }

      $code = isset($_GET['code']) ? $_GET['code'] : '';

      if (empty($code)) {
        throw new InvalidArgumentException('No OAuth Code.');
      }

      $oauth_access_tokon_json_array = open_weixin_get(
        format_string(OPEN_WEIXIN__API__OAUTH2_ACCESS_TOKEN,
          array(
          '@APPID' => $open_weixin_app_id,
          '@APPSECRET' => $open_weixin_app_secret,
          '@CODE' => $code,
          ))
      );
      /* Sample of $oauth_access_tokon_json_array.
        {
          "access_token":"ACCESS_TOKEN",
          "expires_in":7200,
          "refresh_token":"REFRESH_TOKEN",
          "openid":"OPENID",
          "scope":"SCOPE",
          "unionid":""
        }.
      */

      // Get user details from WeChat Server as well as UnionID.
      $oauth_user_info_json_array = open_weixin_get(
        format_string(
          OPEN_WEIXIN__API__OAUTH2_REQUEST_USER_INFO,
          array(
            '@ACCESS_TOKEN' => $oauth_access_tokon_json_array['access_token'],
            '@OPENID' => $oauth_access_tokon_json_array['openid'],
          )
        )
      );
      /* Sample of $oauth_user_info_json_array.
        {
          "openid":"OPENID",
          "nickname":"NICKNAME",
          "sex":1, // 1 Male, 2 Female
          "province":"PROVINCE",
          "city":"CITY",
          "country":"COUNTRY",
          "headimgurl": "http://wx.qlogo.cn/mmopen/g3MonUZtNHkdmzicIlibx6iaFqAc56vxLSUfpb6n5WKSYVY0ChQKkiaJSgQ1dZuTOgvLLrhJbERQQ4eMsv84eavHiaiceqxibJxCfHe/0",
          "privilege":[
          "PRIVILEGE1",
          "PRIVILEGE2"
          ],
          "unionid": " o6_bmasdasdsad6_2sgVt7hMZOPfL"
        }
      */

      // Fetch user information per OpenID from access token JSON.
      // Updated access token will be stored for refresh user details through CRON jobs.
      $oauth2_user = open_weixin_user_from_access_token($open_weixin_app_id, $oauth_access_tokon_json_array);

      // If user exists, profile2: open_weixin exists as well.
      $profile = profile2_load_by_user($oauth2_user, __OPEN_WEIXIN);

    }
    catch (UnexpectedValueException $e) {
      // User doesn't exist per OpenID.
      watchdog_exception(__FUNCTION__, $e, NULL, array(), WATCHDOG_INFO);

      try {
        /* Return User: UnionID exits. */

        $unionid = '';

        if (isset($oauth_access_tokon_json_array['unionid'])) {
          $unionid = $oauth_access_tokon_json_array['unionid'];
        }
        else if (isset($oauth_user_info_json_array['unionid'])) {
          $unionid = $oauth_user_info_json_array['unionid'];
        }

        $oauth2_user = open_weixin_user_by_unionid($unionid);

        // If user exists, bind current WeChat OpenID with retrieved user.
        $profile = profile2_load_by_user($oauth2_user, __OPEN_WEIXIN);

        $connected_wechat = entity_create('field_collection_item', array('field_name' => 'open_weixin_connected_accounts'));
        $connected_wechat->setHostEntity('profile2', $profile);

        open_weixin_update_connected_wechat($connected_wechat, $open_weixin_app_id, $oauth_access_tokon_json_array);
      }
      catch (UnexpectedValueException $e) {
        /* New User, considering Create User or Bind User. */

        // Visitor doesn't exist per UnionID.
        watchdog_exception(__FUNCTION__, $e, NULL, array(), WATCHDOG_INFO);

        $profile = FALSE;

        if (!user_is_logged_in()){
          // Can't find associated user account, create new user account.
          $state = OPEN_WEIXIN__OAUTH2_STATE__REGISTER;

          // Create new user.
          $password = user_password(16);
          $user_fields = array(
            'name' => drupal_random_key(32),
            'pass' => $password,
            'status' => 1,
            'init' => __FUNCTION__,
            'roles' => array(
              DRUPAL_AUTHENTICATED_RID => 'authenticated user',
            ),
            'timezone' => 'Asia/Shanghai',
          );

          $oauth2_user = user_save('', $user_fields);

          user_save($oauth2_user, array(
            'name' => $oauth2_user->uid,
            'mail' => $oauth2_user->uid . '@' . $_SERVER['SERVER_NAME'],
          ));
        }
        else {
          // If user has login-ed, bind received WeChat account with current login account.
          $state = OPEN_WEIXIN__OAUTH2_STATE__BIND;
          
          global $user;
          $profile = profile2_load_by_user($user, __OPEN_WEIXIN);
        }

        if (!$profile) {
          $profile = profile2_create(array(
            'type' => __OPEN_WEIXIN,
            'uid' => $oauth2_user->uid
          ));
          profile2_save($profile);
        }

        // Connect configured WeChat Open Platform account to created user.
        $connected_wechat = entity_create('field_collection_item', array('field_name' => 'open_weixin_connected_accounts'));
        $connected_wechat->setHostEntity('profile2', $profile);

        open_weixin_update_connected_wechat($connected_wechat, $open_weixin_app_id, $oauth_access_tokon_json_array);
      }
    }

    // Stores and update user basic information from WeChat.
    if (isset($profile->open_weixin_user_info)) {
      $open_weixin_user_info = field_collection_item_load($profile->open_weixin_user_info[LANGUAGE_NONE][0]['value']);
    }

    if (empty($open_weixin_user_info)){
      $open_weixin_user_info = entity_create('field_collection_item', array('field_name' => 'open_weixin_user_info'));
      $open_weixin_user_info->setHostEntity('profile2', $profile);
    }

    open_weixin_update_user_info($open_weixin_user_info, $oauth_user_info_json_array);

    global $user;

    if (!user_is_logged_in() || (user_is_logged_in() && $user->uid != $oauth2_user->uid)) {
      $form_state = array();
      $form_state['uid'] = $oauth2_user->uid;

      user_login_submit(array(), $form_state);
      drupal_session_regenerate();
    }

    // Forward visitor to related landing page.
    switch ($state){
      case OPEN_WEIXIN__OAUTH2_STATE__BIND:
        drupal_set_message(t('Your WeChat account has been bound with your Moha Online account.'));
        drupal_goto($open_weixin_landing_login);
        break;
      case OPEN_WEIXIN__OAUTH2_STATE__LOGIN:
        drupal_set_message(t('Welcome Back.'));
        drupal_goto($open_weixin_landing_login);
        break;
      case OPEN_WEIXIN__OAUTH2_STATE__REGISTER:
        drupal_set_message(t('Thank you for your registration.'));
        drupal_goto($open_weixin_langding_register);
        break;
    }

  }
  catch (Exception $e) {
    // Handles all unrecoverable exceptions.
    watchdog_exception(__FUNCTION__, $e);
    print(t(OPEN_WEIXIN__MSG__SERVICE_IS_NA));
  }

  return MENU_SITE_ONLINE;
}
