<?php
/**
 * @file
 */

/**
 * Customized user edit form for updating limited items:
 *   User Name
 *   User Portrait
 *
 * @param $form
 * @param $form_state
 *
 * @return mixed
 */
function open_weixin_user_info($form, &$form_state) {

  global $user;
  $profile = profile2_load_by_user($user, __OPEN_WEIXIN);
  $wrapper = entity_metadata_wrapper('profile2', $profile);

  // User can only change username one time, in case of they make up fake username.
  if ($wrapper->open_weixin_is_named->value() == TRUE){
    $open_weixin_landing_login = open_weixin_array_variable_get(OPEN_WEIXIN__VARIABLES, 'open_weixin_landing_login', 'user');
    drupal_set_message(t('Only new Open Weixin user could change username.'), 'warning');
    drupal_goto($open_weixin_landing_login);
  }

  global $user;
  $current_name = ($user->name === $user->uid)?'':$user->name;

  $form['name'] = array(
    '#title' => t('Username'),
    '#type' => 'textfield',
    '#description' => t('Once chance to customize your username, username must be unique, punctuation is not allowed except for periods, hyphens, apostrophes, and underscores.'),
    '#default_value' => $current_name,
  );

  $form['portrait'] = array(
    '#title' => t('Use portrait from WeChat'),
    '#type' => 'checkbox',
    '#default_value' =>TRUE,
  );

  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save'));

  return $form;
}

function open_weixin_user_info_validate ($form, &$form_state){
  global $user;
  $account = $user;

  if (isset($form_state['values']['name'])) {
    if ($error = user_validate_name($form_state['values']['name'])) {
      form_set_error('name', $error);
    }
    elseif ((bool) db_select('users')->fields('users', array('uid'))->condition('uid', $account->uid, '<>')->condition('name', db_like($form_state['values']['name']), 'LIKE')->range(0, 1)->execute()->fetchField()) {
      form_set_error('name', t('The name %name is already taken.', array('%name' => $form_state['values']['name'])));
    }
  }
}

/**
 *
 * @param $form
 * @param $form_state
 *
 * @throws \Exception
 */
function open_weixin_user_info_submit($form, &$form_state){
  $values = $form_state['values'];

  global $user;

  $profile = profile2_load_by_user($user, __OPEN_WEIXIN);
  $portrait_file = NULL;

  // Fetch portrait from WeChat server.
  if (isset($values['portrait']) && $values['portrait']) {
    try {
      $open_weixin_user_info = field_collection_item_load($profile->open_weixin_user_info[LANGUAGE_NONE][0]['value']);
      $weixin_user_info_wrapper = entity_metadata_wrapper('field_collection_item', $open_weixin_user_info);

      $weixin_portrait_url = $weixin_user_info_wrapper->open_weixin_portrait->value();

      // Fetch URL file to local.
      $portrait_file = media_parse_to_file($weixin_portrait_url);

      // save portrait file to pre-defined folder.
      $uri = file_stream_wrapper_uri_normalize(OPEN_WEIXIN__SCHEME__PORTRAIT . $user->uid . '.jpg');
      $portrait_file = file_move($portrait_file, $uri);

    } catch (Exception $e) {
      watchdog_exception(__FUNCTION__, $e);
    }
  }

  user_save($user, array(
    'name' => $values['name'],
    'picture' => $portrait_file,
  ));

  // Marks current user has specified name.
  $wrapper = entity_metadata_wrapper('profile2', $profile);
  $wrapper->open_weixin_is_named->set(TRUE);
  $wrapper->save(TRUE);

  // Seek login landing page and forward user to it.
  $open_weixin_landing_login = open_weixin_array_variable_get(OPEN_WEIXIN__VARIABLES, 'open_weixin_landing_login', 'user');
  drupal_set_message(t('User data have been saved.'));
  drupal_goto($open_weixin_landing_login);
}
